import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { styled } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import { useStoreState } from '../../store';
import { actions } from '../../store/users';

const StyledFab = styled(Fab)({
  position: "fixed",
  right: 20,
  bottom: 20,
});

const StyledCard = styled(Card)({
  marginBottom: 20,
});

const makeNewUserData = (name: string) => ({
  // id will be generated
  name,
  username: "Foo",
  email: "foo@bar.biz",
  address: {
    street: "Kulas Light",
    suite: "Apt. 556",
    city: "Gwenborough",
    zipcode: "92998-3874",
    geo: {
      lat: "-37.3159",
      lng: "81.1496"
    }
  },
  phone: "1-770-736-8031 x56442",
  website: "hildegard.org",
  company: {
    name: "Romaguera-Crona",
    catchPhrase: "Multi-layered client-server neural-net",
    bs: "harness real-time e-markets"
  }
});

export default function UsersPage() {
  const dispatch = useDispatch();
  const { loading, users } = useStoreState(state => state.users);

  // Load documents on component mount
  useEffect(() => {
    dispatch(actions.loadUsers());
  }, [dispatch]);

  return (
    <>
      <Backdrop open={loading} style={{ zIndex: 10000 }}>
        <CircularProgress color="primary" />
      </Backdrop>
      <StyledFab color="primary" aria-label="add" onClick={() => {
        const name = prompt("Enter user name");

        if (name) {
          dispatch(actions.createUser(makeNewUserData(name)));
        }
      }}>
        <AddIcon />
      </StyledFab>
      {users.map(user => (
        <StyledCard key={user.id}>
          <CardContent>
            <Typography component="pre">
              {JSON.stringify(user, null, 4)}
            </Typography>
          </CardContent>
          <CardActions>
            <Button
              size="small"
              color="primary"
              onClick={() => dispatch(actions.updateUser({ ...user, name: prompt("Enter new name", user.name) ?? user.name }))}
            >
              Change name
            </Button>
            <Button
              size="small"
              color="primary"
              // eslint-disable-next-line no-restricted-globals
              onClick={() => confirm(`Do you really want to delete user ${user.name}?`) && dispatch(actions.deleteUser(user))}
            >
              Delete user
            </Button>
          </CardActions>
        </StyledCard>
      ))}
    </>
  );
}
