import React from 'react';
import { Switch, Redirect, Route } from 'react-router';
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import PeopleIcon from '@material-ui/icons/People';
import Layout from './layout/Layout';
import UsersPage from './users/UsersPage';

const useStyles = makeStyles({
  '@global': {
    'html, body, #root': {
      height: '100vh',
      font: '16px/20px -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
    }
  }
});

interface ListItemLinkProps {
  icon?: React.ReactElement;
  primary: string;
  to: string;
}

function ListItemLink(props: ListItemLinkProps) {
  const { icon, primary, to } = props;

  const renderLink = React.useMemo(
    () =>
      React.forwardRef<any, Omit<RouterLinkProps, 'to'>>((itemProps, ref) => (
        <RouterLink to={to} ref={ref} {...itemProps} />
      )),
    [to],
  );

  return (
    <li>
      <ListItem button component={renderLink}>
        {icon ? <ListItemIcon>{icon}</ListItemIcon> : null}
        <ListItemText primary={primary} />
      </ListItem>
    </li>
  );
}

export default function App() {
  useStyles();

  return (
    <Layout drawer={(
      <List>
        <ListItemLink to="/users" primary="Users" icon={<PeopleIcon />} />
      </List>
    )}>
      <Switch>
        <Redirect exact path="/" to="/users" />
        <Route path="/users/:id" component={UsersPage}  />
        <Route path="/users" component={UsersPage} />
      </Switch>
    </Layout>
  );
}
