import React from 'react';
import ReactDOM from 'react-dom';
import { Provider as ReduxProvider, ReactReduxContext } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import CssBaseline from '@material-ui/core/CssBaseline';
import * as serviceWorker from './serviceWorker';
import configureStore, { history } from './configureStore';
import ConfigContext from './context/ConfigContext';
import App from './components/App';
import appConfig from './config';

// Polyfills (looking at you, Microsoft)
import 'array-flat-polyfill';

const store = configureStore();

ReactDOM.render(
  <ReduxProvider store={store} context={ReactReduxContext}>
    <ConnectedRouter history={history} context={ReactReduxContext}>
      <ConfigContext.Provider value={appConfig}>
        <CssBaseline />
        <App />
      </ConfigContext.Provider>
    </ConnectedRouter>
  </ReduxProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

