import queryString from 'query-string';

const apiBaseUrl = 'https://jsonplaceholder.typicode.com';

export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
  address: {
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: {
      lat: string;
      lng: string;
    }
  },
  phone: string;
  website: string;
  company: {
    name: string;
    catchPhrase: string;
    bs: string;
  }
}

export type UserFilter = Partial<User>;
export type UserWithoutId = Omit<User, "id">;

export default {
  async getUsers(filter: UserFilter): Promise<User[]> {
    const filterString = queryString.stringify(filter);
    const response = await fetch(`${apiBaseUrl}/users?${filterString}`);
    return response.json();
  },
  async getUser(id: number): Promise<User> {
    const response = await fetch(`${apiBaseUrl}/users/${id}`);
    return response.json();
  },
  async createUser(user: UserWithoutId): Promise<User> {
    const response = await fetch(`${apiBaseUrl}/users`, {
      method: 'POST',
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      }
    });
    return response.json();
  },
  async updateUser(user: User): Promise<User> {
    const response = await fetch(`${apiBaseUrl}/users/${user.id}`, {
      method: 'PUT',
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      }
    });
    return response.json();
  },
  async deleteUser(user: User): Promise<void> {
    await fetch(`${apiBaseUrl}/users/${user.id}`, {
      method: 'DELETE',
      headers: {
        "Content-Type": "application/json; charset=UTF-8",
      }
    });
  },
};
