import * as users from './users';
import { useSelector, TypedUseSelectorHook } from 'react-redux';

// The top-level state object
export interface ApplicationState {
  users: users.UsersState;
}

// Whenever an action is dispatched, Redux will update each top-level application state property using
// the reducer with the matching name. It's important that the names match exactly, and that the reducer
// acts on the corresponding ApplicationState property type.
export const reducers = {
  users: users.reducer,
};

// Custom useSelector that prefills state type
export const useStoreState: TypedUseSelectorHook<ApplicationState> = useSelector;
