import { Reducer } from 'redux';
import { ThunkAction } from 'redux-thunk';
import usersService, { User, UserFilter, UserWithoutId } from '../services/users';
import { ApplicationState } from '.';

// State
export interface UsersState {
  loading: boolean;
  users: User[];
}

// Types
const LOAD_USERS = 'LOAD_USERS';
const LOAD_USERS_DONE = 'LOAD_USERS_DONE';
const CREATE_USER = 'CREATE_USER';
const CREATE_USER_DONE = 'CREATE_USER_DONE';
const UPDATE_USER = 'UPDATE_USER';
const UPDATE_USER_DONE = 'UPDATE_USER_DONE';
const DELETE_USER = 'DELETE_USER';
const DELETE_USER_DONE = 'DELETE_USER_DONE';

// Actions
interface LoadUsersAction {
  type: typeof LOAD_USERS;
  filter: Partial<User>;
}
interface LoadUsersDoneAction {
  type: typeof LOAD_USERS_DONE;
  users: User[];
}
interface CreateUserAction {
  type: typeof CREATE_USER;
  user: UserWithoutId;
}
interface CreateUserDoneAction {
  type: typeof CREATE_USER_DONE;
  user: User;
}
interface UpdateUserAction {
  type: typeof UPDATE_USER;
  user: User;
}
interface UpdateUserDoneAction {
  type: typeof UPDATE_USER_DONE;
  user: User;
}
interface DeleteUserAction {
  type: typeof DELETE_USER;
  user: User;
}
interface DeleteUserDoneAction {
  type: typeof DELETE_USER_DONE;
  user: User;
}

export type KnownAction =
  LoadUsersAction |
  LoadUsersDoneAction |
  CreateUserAction |
  CreateUserDoneAction |
  UpdateUserAction |
  UpdateUserDoneAction |
  DeleteUserAction |
  DeleteUserDoneAction;

// Union with void fixes a strange type error
type ThunkResult<Result> = ThunkAction<Result, ApplicationState, undefined, KnownAction> | void;

export const actions = {
  loadUsers: (filter: UserFilter = {}): ThunkResult<void> => {
    return async (dispatch) => {
      dispatch({
        type: LOAD_USERS,
        filter,
      });

      const users = await usersService.getUsers(filter);

      dispatch({
        type: LOAD_USERS_DONE,
        users,
      });
    };
  },
  createUser: (user: UserWithoutId): ThunkResult<void> => {
    return async (dispatch) => {
      dispatch({
        type: CREATE_USER,
        user: user,
      });

      const createdUser = await usersService.createUser(user);

      dispatch({
        type: CREATE_USER_DONE,
        user: createdUser,
      });
    }
  },
  updateUser: (user: User): ThunkResult<void> => {
    return async (dispatch) => {
      dispatch({
        type: UPDATE_USER,
        user: user,
      });

      const createdUser = await usersService.updateUser(user);

      dispatch({
        type: UPDATE_USER_DONE,
        user: createdUser,
      });
    }
  },
  deleteUser: (user: User): ThunkResult<void> => {
    return async (dispatch) => {
      dispatch({
        type: DELETE_USER,
        user: user,
      });

      await usersService.deleteUser(user);

      dispatch({
        type: DELETE_USER_DONE,
        user,
      });
    }
  },
};

// Reducer
const initialState: UsersState = {
  loading: false,
  users: [],
};

export const reducer: Reducer<UsersState, KnownAction> = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_USERS:
      return { ...state, loading: true };

    case LOAD_USERS_DONE:
      return { ...state, loading: false, users: action.users };

    case CREATE_USER:
      return { ...state, loading: true };

    case CREATE_USER_DONE:
      return { ...state, loading: false, users: [...state.users, action.user] };

    case UPDATE_USER:
      return { ...state, loading: true };

    case UPDATE_USER_DONE:
      return {
        ...state,
        loading: false,
        users: state.users.map(user => user.id === action.user.id ? action.user : user),
      };

    case DELETE_USER:
      return { ...state, loading: true };

    case DELETE_USER_DONE:
      return {
        ...state,
        loading: false,
        users: state.users.filter(user => user.id !== action.user.id),
      };

    default:
        // The following line guarantees that every action in the KnownAction
        // union has been covered by a case above
        const exhaustiveCheck: never = action;
        void exhaustiveCheck;
  }

  return state;
};
