import { createBrowserHistory, History } from 'history';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import { routerMiddleware } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import { connectRouter } from 'connected-react-router';
import thunk from 'redux-thunk';
import { reducers } from './store';

const baseUrl = document.querySelector('base')?.getAttribute('href');

export const history = createBrowserHistory({
  basename: baseUrl || undefined,
});

export default function configureStore(preloadedState = {}) {
  const store = createStore(
    createRootReducer(history),
    preloadedState,
    composeWithDevTools(
      applyMiddleware(
        routerMiddleware(history),
        thunk
      )
    )
  );

  return store;
}

function createRootReducer(history: History) {
  return combineReducers({
    ...reducers,
    router: connectRouter(history),
  });
}
